# Ext Manager

Not sure why I kept that name :P. It is just a reference implementation of new Ionic app with simple login and home screen with sidemenu.

### Must knows before start
node latest, python 2.7, ionic, Angularjs, Typescript, Java 8, Android SDK.

### Dev Test in PC

``` 
npm install -g ionic cordova 
cd <project_folder>
npm install
ionic serve
``` 

### Dev Test in Mobile
```

ionic cordova add android
ionic cordova run android
ionic cordova build android

```

### References:
1. Installation: https://ionicframework.com/docs/intro/installation/
2. Deploy: https://ionicframework.com/docs/intro/deploying/
3. Syntax to Edit this file: https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html

### Note
*Minimum SDK Version supported 19.