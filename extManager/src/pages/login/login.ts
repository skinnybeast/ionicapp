import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { HomePage } from '../home/home';
import {AuthenticationService} from '../../service/authentication.service';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  model: any = {};
  awaitingAjaxCall = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    private menu: MenuController, private authenticationService: AuthenticationService) {
    this.menu.enable(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    //reset the awaitingAjaxCall to false once the ajax call is over. This to enable and disable the button
    this.awaitingAjaxCall = true;
    //remove this dummy flow after inegrating with authentication service

    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(result => {
        if(result) {
            this.navCtrl.setRoot(HomePage);
        }
        else {
            let alert = this.alertCtrl.create({
              title: 'Ouch!',
              subTitle: 'Invalid credentials',
              buttons: ['OK']
            });
            alert.present();
        }
        this.awaitingAjaxCall = false;
      }, (error) => {
        this.awaitingAjaxCall = false;
        //Handle error properly
      });
    //call authentication service to allow the user to navigate
    //use alert controller alertCtrl to show error
  }

}
